
import java.util.*;

//Sources:
//http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
//https://www.youtube.com/watch?v=qH6yxkw0u78
//https://www.youtube.com/watch?v=gm8DUJJhmY4
//https://www.youtube.com/watch?v=M6lYob8STMI
//https://inst.eecs.berkeley.edu/~cs61b/sp06/labs/s9-3-1.html
//https://en.wikibooks.org/wiki/Data_Structures/Trees
//https://www.math.upenn.edu/~wilf/AlgoComp.pdf
//https://github.com/kkorvel/home5/blob/master/src/Node.java

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private static LinkedList<String>stringItems;
   
   public static void main (String[] param) 
   {
	      String s = "(B1,C)A";
	      Node t = Node.parsePostfix (s);
	      String v = t.leftParentheticRepresentation();
	      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }//main

   Node (String name, Node firstChild, Node nextSibling) {
	   	this.name = name;
	   	this.firstChild = firstChild;
	   	this.nextSibling = nextSibling;
   }//Node
   
   public static Node parsePostfix (String nodeString) {
      checkForStringErrors(nodeString);
      
      Node root = treeBuild(new Node(null,null,null),stringItems);
      if(root.firstChild == null && root.nextSibling == null)
      {
    	  if(root.name != null)
    	  {
    		  return root;
    	  }
      }
      else if(root.firstChild == null && root.nextSibling !=null)
      {
    	  throw new RuntimeException("No brackets");
      }
      return root;
   }//parsePostfix

   public String leftParentheticRepresentation() {
	   String text = buildTree(this, 0).name;
		return text;
   }
   
   public static void checkForStringErrors(String requiredString)
   {
	  int bracketCounter = 0;
	  stringItems = new LinkedList<String>(Arrays.asList(requiredString.split("")));
	  for(String currentItem : stringItems)
	  {
		  if(currentItem.replaceAll("\\s+", " ").equals(" "))
		  {
			  throw new RuntimeException("Node is empty!");
		  }
		  if(currentItem.equals("("))
		  {
			  bracketCounter++;
		  }
		  else if(currentItem.equals(")"))
		  {
			  bracketCounter--;
			  if(bracketCounter < 0)
			  {
				  throw new RuntimeException("Too many brackets!");
			  }
		  }
		  else if(currentItem.equals(",") && bracketCounter == 0)
		  {
			  throw new RuntimeException("Brackets missing: " + requiredString);
		  }
	    }
   }//checkForErrors
   
   public static Node treeBuild(Node root, LinkedList<String>listItems)
   {
	   while(!listItems.isEmpty())
	   {
		   String listItem = listItems.pop();
		   if(listItem.equals("("))
		   {
			   root.firstChild = treeBuild(new Node(null,null,null),listItems);
		   }
		   else if(listItem.equals(","))
		   {
			   root.nextSibling = treeBuild(new Node(null,null,null),listItems);
			   if(root.name == null)
			   {
				   throw new RuntimeException("empty root");
			   }
			   return root;
		   }
		   else if(listItem.equals(")"))
		   {
			   if(root.name == null)
			   {
				   throw new RuntimeException("empty(X) OR X(empty) OR (empty)X OR (X)empty OR empty(empty)");
			   }
			   return root;
		   }
		   else
		   {
			   if(root.name == null)
			   {
				   root.name = listItem;
			   }
			   else
			   {
				   root.name += listItem;
			   }
		   }
	   }//while
	   if(root.name == null)
	   {
		   throw new RuntimeException("empty, OR ,empty");
	   }
	return root;
   }//treeBuild
 
   public static Node buildTree(Node root, int open) 
   {
		if (root.firstChild != null) 
		{
			open++;
			root.name += "(" + buildTree(root.firstChild, open).name;
			root.name += ")";
			if (root.nextSibling != null) 
			{
				root.name += "," + buildTree(root.nextSibling, open).name;
				return root;
			}
		} else if (root.nextSibling != null) 
		{
			root.name += "," + buildTree(root.nextSibling, open).name;
			return root;
		} else 
		{
			return root;
		}
		return root;
	}//buildTree

}//class Node

